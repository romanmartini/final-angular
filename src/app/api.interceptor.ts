import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { APIREST_HOST } from './env'

import { AuthService } from "./services/auth.service";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    URL:string = APIREST_HOST

    constructor(
        private http: AuthService
    ) { }

    intercept( req: HttpRequest<any>, next: HttpHandler ): Observable<HttpEvent<any>> {

        if( req.url.includes(`${this.URL}/auth`) ) return next.handle( req );
        
        const auth = this.http.getToken();
        const authHeader = `Bearer ${auth}`;

        const authReq = req.clone({
            headers: req.headers.set('Authorization', authHeader)
        });

        return next.handle( authReq );
    }

}