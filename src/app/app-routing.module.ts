import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// guards
import { AuthGuard } from './guards/auth.guard';

// components
import { HomeComponent } from './home/home.component';
import { ErrorComponent } from './error/error.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'error', component: ErrorComponent},
  {path: 'auth', loadChildren: ()=> import('./auth/auth.module').then(m => m.AuthModule)},
  {path: 'clothes', canLoad: [AuthGuard], loadChildren: ()=> import('./clothes/clothes.module').then(m => m.ClothesModule)},
  {path: 'profile', canLoad: [AuthGuard], loadChildren: ()=> import('./profile/profile.module').then(m => m.ProfileModule)},
  {path: '**', redirectTo: 'error', pathMatch: 'full'}
]

@NgModule({
  imports: [ RouterModule.forRoot( routes ) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
