import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// components
import { SinginComponent } from './singin/singin.component';
import { SingupComponent } from './singup/singup.component';

const routes : Routes = [
  {path: 'signin', component: SinginComponent},
  {path: 'signup', component: SingupComponent},
  {path: '**', redirectTo: 'signin', pathMatch: 'full'},
]

@NgModule({
  imports: [ RouterModule.forChild( routes ) ],
  exports: [ RouterModule ]
})
export class AuthRoutingModule { }
