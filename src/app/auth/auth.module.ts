import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

// modules
import { AuthRoutingModule } from './auth-routing.module';
import { ComponentsModule } from '../components/components.module';

// components
import { SinginComponent } from './singin/singin.component';
import { SingupComponent } from './singup/singup.component';



@NgModule({
  declarations: [
    SingupComponent,
    SinginComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    AuthRoutingModule,
    ComponentsModule
  ]
})
export class AuthModule { }
