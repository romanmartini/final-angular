import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

import { AuthService } from 'src/app/services/auth.service';
import { IUserModelSignIn } from 'src/app/interfaces';
import { modalType } from '../../components/modal-message/modal-message.interface';


@Component({
  selector: 'app-singin',
  templateUrl: './singin.component.html',
  styleUrls: ['./singin.component.css']
})
export class SinginComponent implements OnInit {

  /***************************************************************
   * variables to handle modal-message component (modal) 
   **************************************************************/
  modalMessageActive: boolean = false;
  modalCustomTitle: string = '';
  modalCustomMessage: string = ''
  modalType!: modalType;
  // end of variables to handle modals
  
  private subscription!: Subscription;
  signInForm!: FormGroup;

  constructor(
    private authService: AuthService,
    private router: Router,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.createForm();
  }

  /***************************************************************
   * reactive form 
   **************************************************************/
   createForm(){
    this.signInForm = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
    })
  }

  doSubmit(){
    if( this.signInForm.invalid ) {
      this.modalMessageActive = true;
      this.modalType = "error"
      this.modalCustomTitle = 'Error';
      this.modalCustomMessage = "Todos los campos son requeridos"
      return;
    }
    this.subscriptionHandlerToSignIn( this.signInForm.value )
  }

  /*************************************************************
   * handlers for service subscriptions
   ************************************************************/
  subscriptionHandlerToSignIn(userSignIn: IUserModelSignIn){
    this.modalMessageActive = true;
    this.modalType = 'load';
    this.modalCustomTitle = 'Iniciando sesión';
    this.modalCustomMessage = 'Espere por favor'
    this.subscription =  this.authService.signIn(userSignIn).subscribe( 
      () => {},
      (err) => this.errorHandler(err),
      () => this.completedHandler()
    )
  }

  errorHandler( err: any ){
    let code  = err.error.code;
    this.subscription.unsubscribe();
    if( code === 'AUTH-ERR' ) {
      this.modalType = "error"
      this.modalCustomTitle = 'Error';
      this.modalCustomMessage = `Contraseña o email invalidos`;
      return;
    }
    if( code === 'VALIADATION-ERR' ) { 
      this.modalType = "error"
      this.modalCustomTitle = 'Error';
      this.modalCustomMessage = "Todos los campos son requeridos"
      return
    }
    if( code === 'ERR' ) { 
      this.modalType = "error"
      this.modalCustomTitle = 'Error';
      this.modalCustomMessage = "Error en el servidor. Intenta luego"
      return
    }
    this.modalType = 'error'
    this.modalCustomTitle = 'Error';
    this.modalCustomMessage = `Error inesperado`
  }

  completedHandler(){
    this.modalMessageActive = false;
    this.subscription.unsubscribe()
    this.router.navigateByUrl('/profile')
  }
}
