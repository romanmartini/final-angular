import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from 'src/app/services/auth.service';
import { IUserModelSignIn, IUserModelSignUp } from 'src/app/interfaces';
import { modalType } from '../../components/modal-message/modal-message.interface';


@Component({
  selector: 'app-singup',
  templateUrl: './singup.component.html',
  styleUrls: ['./singup.component.css']
})
export class SingupComponent implements OnInit {

  /***************************************************************
   * variables to handle modal-message component (modal) 
   **************************************************************/
  modalMessageActive: boolean = false;
  modalCustomTitle: string = '';
  modalCustomMessage: string = ''
  modalType!: modalType;
  // end of variables to handle modals

  private subscription!: Subscription;
  signUpForm!: FormGroup;

  constructor(
    private authService: AuthService,
    private router: Router,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.createForm();
  }

  /***************************************************************
   * reactive form 
   **************************************************************/
   createForm(){
    this.signUpForm = this.fb.group({
      name: ['', [Validators.required, Validators.pattern( new RegExp('^[a-záéíóúü]+$', 'i') )] ],
      surname: ['', [Validators.required, Validators.pattern( new RegExp('^[a-záéíóúü]+$', 'i') )] ],
      email: ['', [Validators.required, Validators.email ]],
      password: ['', [Validators.required, Validators.pattern( new RegExp('^[a-z0-9áéíóúñü@$!%*?&]{3,30}$', 'i') )]],
    })
  }

  doSubmit(){
    if( this.signUpForm.invalid ) {
      Object.values( this.signUpForm.controls ).forEach( control => control.markAsTouched() )
      this.modalMessageActive = true;
      this.modalType = "error"
      this.modalCustomTitle = 'Error';
      this.modalCustomMessage = "Formulario inválido"
      return;
    }
    this.subscriptionHandlerToSignUp( this.signUpForm.value )
  }

  // metodos to get validations for each input
  get nameIsvalid():boolean | undefined {
    return this.signUpForm.get('name')?.invalid && this.signUpForm.get('name')?.touched
  }
  get surnameIsvalid():boolean | undefined {
    return this.signUpForm.get('surname')?.invalid && this.signUpForm.get('surname')?.touched
  }
  get emailIsvalid():boolean | undefined {
    return this.signUpForm.get('email')?.invalid && this.signUpForm.get('email')?.touched
  }
  get passwordIsvalid():boolean | undefined {
    return this.signUpForm.get('password')?.invalid && this.signUpForm.get('password')?.touched
  }

  /*************************************************************
   * handlers for service subscriptions
   ************************************************************/
  subscriptionHandlerToSignUp(userSignUp: IUserModelSignUp){
    this.modalMessageActive = true;
    this.modalType = 'load';
    this.modalCustomTitle = 'Creando usuario';
    this.modalCustomMessage = 'Espere por favor'
    this.subscription =  this.authService.signUp(userSignUp).subscribe( 
      () => this.nextHandler(),
      (err) => this.errorHandler(err),
    )
  }

  nextHandler(){
    const {email, password} = this.signUpForm.value;
    const userSigIn: IUserModelSignIn = { email, password }
    this.authService.signIn(userSigIn).subscribe(
      () => {},
      (err) => this.errorHandler(err),
      () => this.completedHandler()
    )
  }
  
  errorHandler( err: any ){
    let code  = err.error.code;
    let message = err.error.message;
    this.subscription.unsubscribe();
    if( code === 'VALIADATION-ERR' ) { 
      this.modalType = "error"
      this.modalCustomTitle = 'Error';
      this.modalCustomMessage = "Campos del formulario inválidos"
      return
    }
    if( code === 'ERR' && message.includes('E11000 duplicate') ) { 
      this.modalType = "error"
      this.modalCustomTitle = 'Error';
      this.modalCustomMessage = "El email ya está en uso"
      return
    }
    this.modalType = 'error'
    this.modalCustomTitle = 'Error';
    this.modalCustomMessage = `Error inesperado`
  }

  completedHandler(){
    this.modalMessageActive = false;
    this.subscription.unsubscribe()
    this.router.navigateByUrl('/profile?create=true')
  }
}