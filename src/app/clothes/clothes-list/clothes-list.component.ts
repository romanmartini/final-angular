import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

import { ClothesService } from 'src/app/services/clothes.service';
import { IResClothesData } from 'src/app/interfaces';
import { modalType } from '../../components/modal-message/modal-message.interface';


@Component({
  selector: 'app-clothes-list',
  templateUrl: './clothes-list.component.html',
  styleUrls: ['./clothes-list.component.css']
})
export class ClothesListComponent implements OnInit {

  /***************************************************************
   * variables to handle modal-message component (modal) 
   **************************************************************/
  modalMessageActive: boolean = false;
  modalCustomTitle: string = '';
  modalCustomMessage: string = ''
  modalType!: modalType;
  /***************************************************************
   * variables to handle clothes components  (modal) 
   **************************************************************/
  modalUpdateClothesActive:boolean = false;
  modalDeleteClothesActive: boolean = false;
  // end of variables to handle modals

  private subscription!: Subscription;
  clothesList: IResClothesData[] = [];
  clothes!: IResClothesData;

  constructor(
    private clothesService: ClothesService,
    private router: Router,
  ) { }

  ngOnInit(){
    this.subscriptionHandlerToGetAll()
  }

  /*************************************************************
   * handlers for service subscriptions
   ************************************************************/
  subscriptionHandlerToGetAll(){
    this.modalMessageActive = true;
    this.modalType = "load-page"
    this.subscription =  this.clothesService.getAllClothes().subscribe( 
      (resClothesData) => this.clothesList = resClothesData, 
      (err) => this.errorHandler(err), 
      () => this.completedHandler()
    )
  }
  
  errorHandler( err: any ){
    let code  = err.error.code;
    this.subscription.unsubscribe();
    if( code === 'AUTH-ERR' ) {
      this.router.navigateByUrl(`/error?code=${code}`);
      return;
    }
    if( code === 'VALIADATION-ERR' ) {
      this.modalType = 'error'
      this.modalCustomTitle = 'Error';
      this.modalCustomMessage = err.error.message
      return
    }
    this.modalType = 'error'
    this.modalCustomTitle = 'Error';
    this.modalCustomMessage = `Error inesperado`
  }

  completedHandler(){
    this.subscription.unsubscribe()
    this.modalMessageActive = false;
  }

  /*************************************************************
   * DOM methods and events
   ************************************************************/
  openModalUpdateClothes( clothes: IResClothesData ){
    this.clothes = clothes
    this.modalUpdateClothesActive = true;
  }

  openModalDeleteClothes( clothes: IResClothesData ){
    this.clothes = clothes
    this.modalDeleteClothesActive = true;
  }

  delteClothesList( clothesChildComponent: IResClothesData ) {
    this.clothesList = this.clothesList.filter( clothe => clothe._id !== clothesChildComponent._id )
  }

  updateClothesList( clothesChildComponent: IResClothesData ){ 
    this.clothesList.forEach( (clothes, i) => {
      if(clothes._id === clothesChildComponent._id ){
        this.clothesList[i] = clothesChildComponent;
      }
    })
  }
  // end DOM methods and events
  
  get clothesIsEmpty(){
    return ( this.clothesList.length < 1 ) ? true : false;
  }
}
