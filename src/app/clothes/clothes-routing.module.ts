import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClothesComponent } from './clothes/clothes.component';

// components
import { ClothesListComponent } from './clothes-list/clothes-list.component';
import { CreateClothesComponent } from './create-clothes/create-clothes.component';

const routes : Routes = [
  {path: '', component: ClothesListComponent},
  {path: 'create', component: CreateClothesComponent},
  {path: ':id', component: ClothesComponent},
]

@NgModule({
  imports: [ RouterModule.forChild( routes ) ],
  exports: [ RouterModule ],
})
export class ClothesRoutingModule { }
