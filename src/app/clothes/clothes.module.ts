import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

// modules
import { ClothesRoutingModule } from './clothes-routing.module';
import { ComponentsModule } from '../components/components.module';

// components
import { ClothesComponent } from './clothes/clothes.component';
import { ClothesListComponent } from './clothes-list/clothes-list.component';
import { CreateClothesComponent } from './create-clothes/create-clothes.component';
import { UpdateClothesComponent } from './update-clothes/update-clothes.component';
import { DeleteClothesComponent } from './delete-clothes/delete-clothes.component';

@NgModule({
  declarations: [
    ClothesComponent,
    ClothesListComponent,
    CreateClothesComponent,
    UpdateClothesComponent,
    DeleteClothesComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ClothesRoutingModule,
    ComponentsModule
  ]
})
export class ClothesModule { }
