import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { ClothesService } from 'src/app/services/clothes.service';
import { IResClothesData } from 'src/app/interfaces';
import { modalType } from '../../components/modal-message/modal-message.interface';


@Component({
  selector: 'app-clothes',
  templateUrl: './clothes.component.html',
  styleUrls: ['./clothes.component.css']
})
export class ClothesComponent implements OnInit {

  /***************************************************************
   * variables to handle modal-message component (modal) 
   **************************************************************/
  modalMessageActive: boolean = false;
  modalCustomTitle: string = '';
  modalCustomMessage: string = ''
  modalType!: modalType;
  /***************************************************************
   * variables to handle clothes components  (modal) 
   **************************************************************/
  modalUpdateClothesActive:boolean = false;
  modalDeleteClothesActive: boolean = false;
  // end of variables to handle modals

  private subscription!: Subscription
  clothes: IResClothesData[] = []

  constructor(
    private clothesService: ClothesService,
    private router: Router,
    private activateRoute: ActivatedRoute
  ) { }

  ngOnInit(){
    this.subscriptionHandlerToGetById()
  }

  /*************************************************************
   * handlers for service subscriptions
   ************************************************************/
  subscriptionHandlerToGetById(){
    this.modalMessageActive = true;
    this.modalType = "load-page";
    let paramsId = this.activateRoute.snapshot.params.id
    this.subscription = this.clothesService.getClothesById(paramsId).subscribe( 
      (resClothesData) =>  this.clothes[0] = resClothesData,
      (err) => this.errorHandler(err), 
      () => this.completedHandler()
    )
  }

  errorHandler( err: any ){
    let code  = err.error.code;
    this.subscription.unsubscribe();
    if( code === 'AUTH-ERR' ) {
      this.router.navigateByUrl(`/error?code=${code}`);
      return;
    }
    if( code === 'NOT-FOUND' || code === 'ERR' ) {
      this.modalType = 'error'
      this.modalCustomTitle = 'Producto inexistente'
      this.modalCustomMessage = `El producto no existe`
      return
    }
    this.modalType = 'error'
    this.modalCustomTitle = 'Error';
    this.modalCustomMessage = `Error inesperado`
  }

  completedHandler(){
    this.subscription.unsubscribe()
    this.modalMessageActive = false;
  }

  /***********************************************************
   * DOM methods and events
   ************************************************************/
  openModalUpdateClothes( clothes: IResClothesData ){
    this.clothes[0] = clothes
    this.modalUpdateClothesActive = true;
  }

  openModalDeleteClothes( clothes: IResClothesData ){
    this.clothes[0] = clothes
    this.modalDeleteClothesActive = true;
  }

  delteClothesList( clothesChildComponent: IResClothesData ) {
    this.clothes = this.clothes.filter( clothe => clothe._id !== clothesChildComponent._id )
  }

  updateClothesList( clothesChildComponent: IResClothesData ){ 
    this.clothes.forEach( (clothes, i) => {
      if(clothes._id === clothesChildComponent._id ){
        this.clothes[i] = clothesChildComponent;
      }
    })
  }
  // end DOM methods and events

  get clothesIsEmpty(){
    return ( this.clothes.length < 1 ) ? true : false;
  }
}
