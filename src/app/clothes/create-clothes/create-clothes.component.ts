import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ClothesService } from 'src/app/services/clothes.service';
import { IClothesModel, IResClothesData } from 'src/app/interfaces';
import { clothesCategories } from 'src/app/interfaces/clothes.interface';
import { modalType } from '../../components/modal-message/modal-message.interface';


@Component({
  selector: 'app-create-clothes',
  templateUrl: './create-clothes.component.html',
  styleUrls: ['./create-clothes.component.css']
})
export class CreateClothesComponent implements OnInit {

  /***************************************************************
   * variables to handle modal-message component (modal) 
   **************************************************************/
  modalMessageActive: boolean = false;
  modalCustomTitle: string = '';
  modalCustomMessage: string = ''
  modalType!: modalType;

  private subscription!: Subscription;
  clothesForm!: FormGroup;
  clothesCategoties: clothesCategories[] = ['diver', 't-shirt', 'jacket', 'pants'];

  constructor(
    private clothesService: ClothesService,
    private router: Router,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.createForm();
  }

  /***************************************************************
   * reactive form 
   **************************************************************/
  createForm(){
    this.clothesForm = this.fb.group({
      description: ['', Validators.required],
      category: ['', Validators.required],
      price: ['', [ Validators.required, Validators.min(0)] ],
      stock: ['', [ Validators.required, Validators.min(0)] ],
    })
  }

  resetForm(){
    this.clothesForm.reset({
      description: '',
      category: '',
      price: '',
      stock: ''
    })
  }

  doSubmit(){
    if( this.clothesForm.invalid ) {
      Object.values( this.clothesForm.controls ).forEach( control => control.markAsTouched() )
      this.modalMessageActive = true;
      this.modalType = "error"
      this.modalCustomTitle = "Error"
      this.modalCustomMessage = "Formulario inválido"
      return;
    }
    this.subscriptionHandlerToCreate( this.clothesForm.value )
  }

  // metodos to get validations for each input
  get descriptionIsvalid():boolean | undefined {
    return this.clothesForm.get('description')?.invalid && this.clothesForm.get('description')?.touched
  }
  get categoryIsvalid():boolean | undefined {
    return this.clothesForm.get('category')?.invalid && this.clothesForm.get('category')?.touched
  }
  get stockIsvalid():boolean | undefined {
    return this.clothesForm.get('stock')?.invalid && this.clothesForm.get('stock')?.touched
  }
  get priceIsvalid():boolean | undefined {
    return this.clothesForm.get('price')?.invalid && this.clothesForm.get('price')?.touched
  }

  /*************************************************************
   * handlers for service subscriptions
   ************************************************************/
  subscriptionHandlerToCreate(clothes: IClothesModel){
    this.modalMessageActive = true;
    this.modalType = 'load';
    this.modalCustomTitle = "Creando"
    this.modalCustomMessage = `Espere por favor`;
    this.subscription =  this.clothesService.createClothes(clothes).subscribe( 
      (resColthesData) => this.nextHandler(resColthesData),
      (err) => this.errorHandler(err),
      () => this.completedHandler()
    )
  }

  nextHandler(clothes: IResClothesData){
    this.modalType = 'success';
    this.modalCustomTitle = "Producto creado"
    this.modalCustomMessage = `El producto "${clothes.description}" fue creado`;
  }
  
  errorHandler( err: any ){
    let code  = err.error.code;
    this.subscription.unsubscribe();
    if( code === 'AUTH-ERR' ) {
      this.router.navigateByUrl(`/error?code=${code}`);
      return;
    }
    if( code === 'VALIADATION-ERR' ) {
      this.modalType = 'error'
      this.modalCustomTitle = "Error"
      this.modalCustomMessage = err.error.message
      return
    }
    this.modalType = 'error'
    this.modalCustomTitle = 'Error';
    this.modalCustomMessage = `Error inesperado`
  }

  completedHandler(){
    this.resetForm()
    this.subscription.unsubscribe()
  }
}
