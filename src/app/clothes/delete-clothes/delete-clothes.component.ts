import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { ClothesService } from 'src/app/services/clothes.service';
import { IResClothesData } from 'src/app/interfaces';
import { modalType } from '../../components/modal-message/modal-message.interface';


@Component({
  selector: 'app-delete-clothes',
  templateUrl: './delete-clothes.component.html',
  styleUrls: ['./delete-clothes.component.css']
})
export class DeleteClothesComponent implements OnInit {

  /***************************************************************
   * variables to handle the modal-message component (modal) 
   **************************************************************/
  modalMessageActive: boolean = false;
  modalCustomTitle: string = '';
  modalCustomMessage: string = ''
  modalType!: modalType;
  // end variables to handle modal-message
  
  @Input() clothes!: IResClothesData;
  @Output() showModal: EventEmitter<boolean> = new EventEmitter()
  @Output() chlothesDelete: EventEmitter<IResClothesData> = new EventEmitter()
  
  private subscription!: Subscription;
  clothesId!: string;

  constructor(
    private clothesService: ClothesService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.clothesId = this.clothes._id;
    this.openModalToConfirmDeleteClothes()
  }

  /*************************************************************
   * handlers for service subscriptions
   ************************************************************/
  subscriptionHandlerToDelete(id: string){
    this.modalMessageActive = true;
    this.modalType = "load"
    this.modalCustomTitle = "Eliminando"
    this.modalCustomMessage = `Espere por favor`
    this.subscription =  this.clothesService.deleteClothes(id).subscribe( 
      () => this.nextHandler(),
      (err) => this.errorHandler(err),
      () => this.completedHandler()
    )
  }

  nextHandler(){
    this.chlothesDelete.emit(this.clothes)
    this.modalType = 'success'
    this.modalCustomTitle = "Producto Eliminado"
    this.modalCustomMessage = `El producto "${this.clothes.description}" fue eliminado`
  }
  
  errorHandler( err: any ){
    let code  = err.error.code;
    this.subscription.unsubscribe();
    if( code === 'AUTH-ERR' ) {
      this.router.navigateByUrl(`/error?code=${code}`);
      return;
    }
    if( code === 'NOT-FOUND' || code === 'ERR' ) {
      this.modalType = 'error'
      this.modalCustomTitle = 'Producto inexistente'
      this.modalCustomMessage = `El producto no existe`
      return
    }
    this.modalType = 'error'
    this.modalCustomTitle = 'Error';
    this.modalCustomMessage = `Error inesperado`
  }

  completedHandler(){
    this.subscription.unsubscribe()
  }

  /*************************************************************
   * DOM methods and events
   ************************************************************/
  openModalToConfirmDeleteClothes(){
    this.modalMessageActive = true;
    this.modalType = 'delete';
    this.modalCustomTitle = `Emiminar`
    this.modalCustomMessage = `¿Seguro quieres elimiar "${this.clothes.description}"?`
  }

  confirmDelete( value: boolean){
    if( value )this.subscriptionHandlerToDelete(this.clothes._id);
    else this.closeDeleteModal()
  }
  
  closeDeleteModal( value?: boolean ){
    this.modalMessageActive = false; 
    this.showModal.emit(false)
  }
  // fin DOM events
}
