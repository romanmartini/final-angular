import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { ClothesService } from 'src/app/services/clothes.service';
import { modalType } from '../../components/modal-message/modal-message.interface';
import { IClothesModel, IResClothesData } from 'src/app/interfaces';
import { clothesCategories } from 'src/app/interfaces/clothes.interface';

@Component({
  selector: 'app-update-clothes',
  templateUrl: './update-clothes.component.html',
  styleUrls: ['./update-clothes.component.css']
})
export class UpdateClothesComponent implements OnInit {

  /***************************************************************
   * variables to handle the modal-message component (modal) 
   **************************************************************/
  modalMessageActive: boolean = false;
  modalCustomTitle: string = '';
  modalCustomMessage: string = ''
  modalType!: modalType;
  
  @Input() clothes!: IResClothesData;
  @Output() showModal: EventEmitter<boolean> = new EventEmitter()
  @Output() chlothesChange: EventEmitter<IResClothesData> = new EventEmitter()
  
  private subscription!: Subscription;
  clothesForm!: FormGroup;
  clothesCategoties:clothesCategories[] = ['diver', 't-shirt', 'jacket', 'pants'];
  clothesId!: string;

  constructor(
    private clothesService: ClothesService,
    private router: Router,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.clothesId = this.clothes._id
    this.createForm();
    this.resetForm();
  }

  /***************************************************************
   * reactive form 
   **************************************************************/
  createForm(){
    this.clothesForm = this.fb.group({
      description: ['', Validators.required],
      category: ['', Validators.required],
      price: ['', [Validators.required, Validators.min(0)] ],
      stock: ['', [Validators.required, Validators.min(0)] ],
    })
  }

  resetForm(){
    this.clothesForm.reset({
      description: this.clothes.description,
      category: this.clothes.category,
      price: this.clothes.price,
      stock: this.clothes.stock
    })
  }

  doSubmit(){
    if( this.clothesForm.invalid ) {
      Object.values( this.clothesForm.controls ).forEach( control => control.markAsTouched() )
      this.modalMessageActive = true;
      this.modalType = "error"
      this.modalCustomTitle = "Error"
      this.modalCustomMessage = "Formulario inválido"
      return;
    }
    this.subscriptionHandlerToUpdate( this.clothesForm.value )
  }

  // metodos to get validations for each input
  get descriptionIsvalid():boolean | undefined {
    return this.clothesForm.get('description')?.invalid && this.clothesForm.get('description')?.touched
  }
  get categoryIsvalid():boolean | undefined {
    return this.clothesForm.get('category')?.invalid && this.clothesForm.get('category')?.touched
  }
  get stockIsvalid():boolean | undefined {
    return this.clothesForm.get('stock')?.invalid && this.clothesForm.get('stock')?.touched
  }
  get priceIsvalid():boolean | undefined {
    return this.clothesForm.get('price')?.invalid && this.clothesForm.get('price')?.touched
  }

  /*************************************************************
   * handlers for service subscriptions
   ***********************************************************/
  subscriptionHandlerToUpdate(clothes: IClothesModel){
    this.modalMessageActive = true;
    this.modalType = 'load';
    this.modalCustomTitle = "Actualizando"
    this.modalCustomMessage = `Espere por favor`
    this.subscription =  this.clothesService.updateClothes(this.clothesId, clothes).subscribe( 
      (resColthesData) => this.nextHandler(resColthesData),
      (err) => this.errorHandler(err),
      () => this.completedHandler()
    )
  }

  nextHandler(clothes: IResClothesData){
    this.chlothesChange.emit(clothes)
    this.modalType = 'success'
    this.modalCustomTitle = "Producto actualizado"
    this.modalCustomMessage = `El producto "${clothes.description}" fue actualizado`
  }
  
  errorHandler( err: any ){
    let code  = err.error.code;
    let status  = err.status;
    this.subscription.unsubscribe();
    if( code === 'AUTH-ERR' ) {
      this.router.navigateByUrl(`/error?code=${code}`);
      return;
    }
    if( code === 'VALIADATION-ERR' ) {
      this.modalType = 'error'
      this.modalCustomMessage = err.error.message
      return
    }
    this.modalType = 'error'
    this.modalCustomTitle = 'Error';
    this.modalCustomMessage = `Error inesperado`
  }

  completedHandler(){
    this.subscription.unsubscribe()
  }

  /*************************************************************
   * DOM events
   ************************************************************/
  closeUpdateModal(){
    this.showModal.emit(false)
  }
  // fin DOM events

}
