import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// components
import { ModalMessageComponent } from './modal-message/modal-message.component';

@NgModule({
  declarations: [
    ModalMessageComponent,
  ],
  exports: [
    ModalMessageComponent
  ],
  imports: [
    CommonModule
  ]
})
export class ComponentsModule { }
