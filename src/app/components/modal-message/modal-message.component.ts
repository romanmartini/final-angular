import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { modalType } from './modal-message.interface';

@Component({
  selector: 'app-modal-message',
  templateUrl: './modal-message.component.html',
  styleUrls: ['./modal-message.component.css']
})
export class ModalMessageComponent implements OnInit, OnChanges {


  @Input() type!: modalType;
  @Input() title: string = '';
  @Input() text: string = '';
  
  @Output() modalActive: EventEmitter<boolean> = new EventEmitter()
  @Output() confirmDelete: EventEmitter<boolean> = new EventEmitter()

  iconType!: string;
  buttonType!: string;
  buttonText!: string;

  ngOnInit(){
    this.configModal()
  }
  ngOnChanges(){
    this.configModal()
  }

  closeModal(){
    return this.modalActive.emit(false)
  }

  confirm(){
    return this.confirmDelete.emit(true)
  }

  cancel(){
    this.closeModal()
    return this.confirmDelete.emit(false)
  }

  configModal(){
    let iconStyle = 'lg'
    if( this.type === 'load' ) {
      this.iconType = '';
      this.buttonType = `border-info text-info`;
      this.buttonText = '';
    }
    if( this.type === 'success' ) {
      this.iconType = `bi bi-check-${iconStyle} text-success`;
      this.buttonType = `btn-outline-success w-50`;
      this.buttonText = 'Ok';
    }
    if( this.type === 'error' ) {
      this.iconType = `bi bi-x-${iconStyle} text-danger`;
      this.buttonType = `btn-outline-danger w-50`;
      this.buttonText = 'Ok';
    }
    if( this.type === 'delete' ) {
      this.iconType = `bi bi-exclamation-${iconStyle} text-warning`;
      this.buttonType = `btn-outline-warning`;
      this.buttonText = 'Eliminar';
    }
  }

  get getType(){
    return this.type;
  }
}

