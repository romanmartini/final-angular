import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.css']
})
export class ErrorComponent {

  errorMessage!: IErrorMessage;

  constructor(
    private activateRoute: ActivatedRoute,
    private authService: AuthService
  ) { 
    this.setMessageError()
  }

  setMessageError(){
    let queryParams = this.getQueryParams()
    if( queryParams && queryParams.code === 'AUTH-ERR') {
      this.authService.updateAuthStatus(false);
      this.errorMessage = {
        code: '401',
        title: 'Error de autenticación',
        message: 'El token ya no es válido',
        link: 'Iniciar sesión',
        path: '/auth/signin'
      }
    }else{
      this.errorMessage = {
        code: '404',
        title: 'La página que intentas solicitar no se encuentra en el servidor',
        link: 'Volver a la página principal',
        path: '/'
      }
    }
  }

  getQueryParams(){
    let queryParams = this.activateRoute.snapshot.queryParams;
    if( !queryParams.code ) return null;
    return queryParams
  }
}

interface IErrorMessage {
  code?: string;
  title?: string;
  message?: string;
  path?: string;
  link?: string;
}