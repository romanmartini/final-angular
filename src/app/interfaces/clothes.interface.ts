/* Interfaces request
========================================= */
export interface IClothesModel {
    category: clothesCategories;
    stock: number;
    price: number;
    description: string;
}

/* Interfaces response
========================================= */
export interface IResClothesData {
    _id: string;
    category: string;
    stock: number;
    price: number;
    description: string;
}

export type clothesCategories = 'diver' | 't-shirt' | 'jacket' | 'pants';