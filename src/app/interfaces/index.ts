export { IUserModelSignUp, IUserModelSignIn, IResAuthUserData, IResUserData } from './user.interfaces';
export { IClothesModel, IResClothesData } from './clothes.interface'

// <T>: IResAuthUserData | IResUserData | IResClothesData
export interface IApiResponse<T> {
    code: string;
    message: string | null;
    success: boolean;
    data: T;
}
