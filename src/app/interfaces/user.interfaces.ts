/* Interfaces request
========================================= */
export interface IUserModelSignUp {
    name: string;
    surname: string;
    password: string;
    email: string;
    role?: 'ADMIN' | 'USER' | 'EMPLOYEE';
}

export interface IUserModelSignIn {
    email: string;
    password: string;
}

/* Interfaces response
========================================= */
export interface IResAuthUserData {
    user: IResUserData;
    token: string;
}

export interface IResUserData {
    role: string;
    _id: string;
    name: string;
    surname: string;
    email: string;
}