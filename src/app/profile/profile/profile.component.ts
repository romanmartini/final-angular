import { Component, OnInit } from '@angular/core';

import { UserService } from 'src/app/services/user.service';
import { IResUserData } from 'src/app/interfaces';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {


  userProfile!: IResUserData;

  constructor(
    private userService: UserService,
  ) { }

  ngOnInit(): void {
    this.getUserData()
  }

  getUserData(){
    this.userProfile = this.userService.getUserDataLocalSotorage()
  }
}
