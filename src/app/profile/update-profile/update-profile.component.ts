import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

import { modalType } from 'src/app/components/modal-message/modal-message.interface';
import { IUserModelSignUp, IResUserData } from 'src/app/interfaces';
import { AuthService } from 'src/app/services/auth.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-update-profile',
  templateUrl: './update-profile.component.html',
  styleUrls: ['./update-profile.component.css']
})
export class UpdateProfileComponent implements OnInit {

  /***************************************************************
   * variables to handle modal-message component (modal) 
   **************************************************************/
   modalMessageActive: boolean = false;
   modalCustomTitle: string = '';
   modalCustomMessage: string = ''
   modalType!: modalType;
   // end of variables to handle modals

   userProfile!: IResUserData;
   idProfile!: string;
   private subscription!: Subscription;
   userForm!: FormGroup;
 
   constructor(
     private authService: AuthService,
     private userService: UserService,
     private fb: FormBuilder
   ) { }
 
   ngOnInit(): void {
     this.createForm();
     this.getUserData();
     this.resetForm();
   }
 
   /***************************************************************
    * reactive form 
    **************************************************************/
    createForm(){
     this.userForm = this.fb.group({
       name: ['', [Validators.required, Validators.pattern( new RegExp('^[a-záéíóúü]+$', 'i') )] ],
       surname: ['', [Validators.required, Validators.pattern( new RegExp('^[a-záéíóúü]+$', 'i') )] ],
       email: ['', [Validators.required, Validators.email ]],
       password: ['', [Validators.required, Validators.pattern( new RegExp('^[a-z0-9áéíóúñü@$!%*?&]{3,30}$', 'i') )]],
       role: ['', [Validators.required ]],
     })
   }

   resetForm(){
    this.userForm.reset({
      name: this.userProfile.name,
      surname: this.userProfile.surname,
      email: this.userProfile.email,
      password: '',
      role: 'ADMIN'
    })
  }
 
   doSubmit(){
     if( this.userForm.invalid ) {
       Object.values( this.userForm.controls ).forEach( control => control.markAsTouched() )
       this.modalMessageActive = true;
       this.modalType = "error"
       this.modalCustomTitle = 'Error';
       this.modalCustomMessage = "Formulario inválido"
       return;
     }
     this.subscriptionHandlerToUpdateUser(this.idProfile, this.userForm.value )
   }
 
   // metodos to get validations for each input
   get nameIsvalid():boolean | undefined {
     return this.userForm.get('name')?.invalid && this.userForm.get('name')?.touched
   }
   get surnameIsvalid():boolean | undefined {
     return this.userForm.get('surname')?.invalid && this.userForm.get('surname')?.touched
   }
   get emailIsvalid():boolean | undefined {
     return this.userForm.get('email')?.invalid && this.userForm.get('email')?.touched
   }
   get passwordIsvalid():boolean | undefined {
     return this.userForm.get('password')?.invalid && this.userForm.get('password')?.touched
   }
 
  /*************************************************************
  * handlers for service subscriptions
  ************************************************************/
  subscriptionHandlerToUpdateUser(id: string, user: IUserModelSignUp){
    this.modalMessageActive = true;
    this.modalType = 'load';
    this.modalCustomTitle = 'Actualizando usuario';
    this.modalCustomMessage = 'Espere por favor'
    this.subscription =  this.userService.updateUser(id ,user).subscribe( 
      (res) => this.authService.saveUserData(res),
      (err) => this.errorHandler(err),
      () => this.completedHandler()
    )
  }

  errorHandler( err: any ){
    console.log(err)
    let code  = err.error.code;
    let message = err.error.message;
    this.subscription.unsubscribe();
    if( code === 'VALIADATION-ERR' ) { 
      this.modalType = "error"
      this.modalCustomTitle = 'Error';
      this.modalCustomMessage = "Campos del formulario inválidos"
      return
    }
    if( code === 'ERR' && message.includes('E11000 duplicate') ) { 
      this.modalType = "error"
      this.modalCustomTitle = 'Error';
      this.modalCustomMessage = "El email ya está en uso"
      return
    }
    this.modalType = 'error'
    this.modalCustomTitle = 'Error';
    this.modalCustomMessage = `Error inesperado`
  }
 
  completedHandler(){
    this.modalType = 'success'
    this.modalCustomTitle = "Perfil actualizado"
    this.modalCustomMessage = `Tu perfil fue actualizado`
    this.authService.updateAuthStatus(true);
      this.subscription.unsubscribe()
  }

  getUserData(){
    this.userProfile = this.userService.getUserDataLocalSotorage()
    this.idProfile = this.userProfile._id
  }

}
