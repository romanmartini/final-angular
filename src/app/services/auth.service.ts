import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient} from '@angular/common/http'
import { map } from 'rxjs/operators'
import { APIREST_HOST } from '../env'

// interfaces 
import { IUserModelSignIn, IUserModelSignUp, IApiResponse, IResAuthUserData, IResUserData } from '../interfaces';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  
  URL:string = `${APIREST_HOST}/auth`
  authStatus = new EventEmitter<boolean>()

  constructor(
    private http: HttpClient
  ) { }

  signIn( userSignIn: IUserModelSignIn ){
    return this.http.post<IApiResponse<IResAuthUserData>>( `${this.URL}/signin`, userSignIn)
            .pipe( 
              map( res => {
                this.saveToken( res.data.token )
                this.saveUserData( res.data.user )
                this.updateAuthStatus(true);
                return res.data 
              }) 
            );
  }

  signUp( userSignUp: IUserModelSignUp ){
    return this.http.post<IApiResponse<IResUserData>>( `${this.URL}/signup`, userSignUp)
            .pipe( map( res => res.data ));
  }

  updateAuthStatus( value: boolean ){
    if( !value ) this.logout();
    this.authStatus.emit(value);
  }

  private logout(){
    localStorage.removeItem('token');
    localStorage.removeItem('user');
  }
  
  private saveToken( token: string ){
    localStorage.setItem('token', token)
  }

  get userIsAuth(){
    if( this.getToken() ) return true;
    return false
  }

  getToken(){
    return localStorage.getItem('token')
  }

  saveUserData( user: IResUserData ){
    localStorage.setItem('user', JSON.stringify(user) )
  }
}
