import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators'
import { APIREST_HOST } from '../env'

// interfaces
import { IClothesModel, IApiResponse, IResClothesData } from '../interfaces';

@Injectable({
  providedIn: 'root'
})
export class ClothesService {

  URL:string = `${APIREST_HOST}/clothes`

  constructor(
    private http: HttpClient
  ) { }

  getAllClothes(){
    return this.http.get<IApiResponse<IResClothesData[]>>( this.URL )
            .pipe( map(res => res.data) )
  }

  getClothesById( _id: string ){
    return this.http.get<IApiResponse<IResClothesData>>(`${this.URL}/${_id}`)
            .pipe( map(res => res.data) )
  }

  createClothes( clothes: IClothesModel ){
    return this.http.post<IApiResponse<IResClothesData>>(`${this.URL}`, clothes )
              .pipe( map(res => res.data) )
  }

  updateClothes( _id: string, clothes: IClothesModel ){
    return this.http.put<IApiResponse<IResClothesData>>( `${this.URL}/${_id}`, clothes )
            .pipe( map(res => res.data) )
  }

  deleteClothes( _id: string ){
    return this.http.delete(`${this.URL}/${_id}`);
  }

}
