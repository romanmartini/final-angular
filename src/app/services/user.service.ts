import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators'
import { APIREST_HOST } from '../env'

import { IApiResponse, IResUserData, IUserModelSignUp as IUserModel } from '../interfaces';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  URL:string = `${APIREST_HOST}/user`

  constructor(
    private http: HttpClient
  ) { }

  getUserById( _id?: string ){
    return this.http.get<IApiResponse<IResUserData>>( `${this.URL}/${_id}` )
            .pipe( map(res => res.data) )
  }

  updateUser( _id: string, user: IUserModel ){
    return this.http.put<IApiResponse<IResUserData>>( `${this.URL}/${_id}`, user )
            .pipe( map(res => res.data) )
  }

  deleteUser( _id: string ){
    return this.http.delete(`${this.URL}/${_id}`);
  }

  getUserDataLocalSotorage(){
    if( localStorage.getItem('user') ){
      let data: any = localStorage.getItem('user');
      return JSON.parse( data )
    }
    return null;
  }

}
