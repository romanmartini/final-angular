import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { AuthService } from 'src/app/services/auth.service';
import { UserService } from 'src/app/services/user.service';
import { IResUserData } from 'src/app/interfaces';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  userIsAuth!: boolean;
  subscriptionStatusAuth!: Subscription;
  userProfile!: IResUserData;

  constructor(
    private authService: AuthService,
    private userService: UserService,
    private router: Router
  ) { }

  ngOnInit(){
    this.getAuthStatus()
    this.listeningAuthStatus();
    this.getUserData()
  }

  listeningAuthStatus(){
    this.subscriptionStatusAuth =  this.authService.authStatus.subscribe( 
      (res: boolean) => {
        this.userIsAuth = res
        this.getUserData()
      },
    )
  }

  getAuthStatus(){
    this.userIsAuth = this.authService.userIsAuth
  }

  closeSession(){
    this.authService.updateAuthStatus(false);
    this.router.navigateByUrl('/auth/sigin')
  }

  getUserData(){
    this.userProfile = this.userService.getUserDataLocalSotorage()
  }


}
